# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define malin = Character("Malin")

init:
    image seagull = Animation("seagull/seagull00.png", 0.2,
    "seagull/seagull01.png", 0.05,
    "seagull/seagull02.png", 0.05,
    "seagull/seagull03.png", 0.05,
    "seagull/seagull04.png", 0.05,
    "seagull/seagull05.png", 0.05,
    "seagull/seagull06.png", 0.05,
    "seagull/seagull07.png", 0.05,
    "seagull/seagull08.png", 0.05,
    "seagull/seagull09.png", 0.05)

# The game starts here.

label start:

    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.

    stop music fadeout 1
    scene bg pantai
    play sound "sfx/ombak.mp3"

    # This shows a character sprite. A placeholder is used, but you can
    # replace it by adding a file named "eileen happy.png" to the images
    # directory.

    show malin with dissolve
    show seagull behind malin:
        yalign 0.1
        xalign 1.2
        linear 20 xalign -0.5
        repeat

    # These display lines of dialogue.

    "Hello, world."

    show malin ngomong

    malin "Apakah kalian melihat burung?"

    show malin
    malin "......"

    show malin garuk

    malin "Wah, kalian melihatnya!"

    show malin ngomong
    malin "Once you add a story, pictures, and music, you can release it to the world!"

    # This ends the game.
    stop sound

    return
